import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from 'src/app/models/User';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  titulo = 'Registro';
  registerForm: FormGroup;
  user: User;

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router,
              private toastr: ToastrService) { }

  ngOnInit() {
  }

  validation() {
    this.registerForm = this.fb.group({
      fullName: ['', Validators.required],
      email: ['', Validators.required],
      userName: ['', Validators.required],
      passwords: this.fb.group({
        password: ['', [Validators.required, Validators.minLength(6)]],
        confirmPassword: ['', Validators.required]
      }, {validators: this.compararSenhas})
    });
  }

  compararSenhas(fg: FormGroup) {
    // this.registerForm.get('confirmPassword');
    const confirmPassword = fg.get('confirmPassword');

    /*
      se colocou dados nos campos password e confirmPassword com comprimento mínimo de
      6 caracteres, satisfaz a condição
    */
    if (confirmPassword.errors == null || 'mismatch' in confirmPassword.errors) {

      if (fg.get('password').value !== confirmPassword.value) {
        confirmPassword.setErrors({mismatch: true});
      } else {
        confirmPassword.setErrors(null);
      }
    }
  }

  cadastrarUsuario() {
    if (this.registerForm.valid) {
      /*
        como password está dentro de um agrupador (passwords) o seu dado não vem junto com
        this.registerForm.value
      */
      this.user = Object.assign({passowrd: this.registerForm.get('passwords.password').value},
        this.registerForm.value);

      this.authService.register(this.user).subscribe(
        () => {
          this.router.navigate(['/user/login']);
          this.toastr.success('Registro realizado com sucesso!');
        },
        error => {
          const erro = error.error;
          erro.foreach(element => {
            switch (element.code) {
              case 'DuplicateUserName':
                this.toastr.error('Cadastro duplicado');
                break;
              default:
                this.toastr.error(`Erro no cadastro! Código: ${element.code}`);
                break;
            }
          });
        }
      );
    }
  }
}
