import { Component, OnInit } from '@angular/core';
import { EntretenimentoGeneroService } from '../../services/entretenimentoGenero.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-entretenimento-genero',
  templateUrl: './entretenimentoGenero.component.html',
  styleUrls: ['./entretenimentoGenero.component.css']
})
export class EntretenimentoGeneroComponent implements OnInit {

  titulo = 'Entretenimentos E Gêneros';
  entretenimentos: any = [];
  generos: any = [];
  entretenimentoSelecionado: any = {};
  generoSelecionado: any = {};
  entretenimentoGenero: any = {};

  constructor(private entretenimentoGeneroService: EntretenimentoGeneroService,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.getEntretenimentosGeneros();
  }

  getEntretenimentosGeneros() {
    this.entretenimentoGeneroService.getEntretenimentosGeneros().subscribe(
      (response: any) => {
        this.entretenimentos = response['entretenimentos'];
        this.generos         = response['generos'];
        this.toastr.success('Entretenimentos e Gêneros buscados com sucesso!');
      },
      error => this.toastr.error('Não foi possível buscar os entretenimentos e/ou gêneros!')
    );
  }

  salvarEntretenimentoGenero() {

    this.entretenimentoGenero = {entretenimentoId: this.entretenimentoSelecionado.id,
      generoId: this.generoSelecionado.id};

    this.entretenimentoGeneroService.postEntretenimentoGenero(this.entretenimentoGenero).subscribe(
      () => {
        this.toastr.success('Salvo com sucesso!');
        // this.router.navigate(['/entretenimentoGenero']);
      },

      error => this.toastr.error('Erro ao salvar!')
    );
  }
}
