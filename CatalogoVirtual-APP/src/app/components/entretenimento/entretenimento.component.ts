import { Component, OnInit } from '@angular/core';
import { EntretenimentoService } from '../../services/entretenimento.service';
import { Entretenimento } from '../../models/Entretenimento';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { trigger, state, style, animate, transition } from '@angular/animations';

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-entretenimento',
  templateUrl: './entretenimento.component.html',
  styleUrls: ['./entretenimento.component.css'],
  animations: [
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(1000)),
    ]),
  ]
})
export class EntretenimentoComponent implements OnInit {

  tituloEntretenimento = 'Entretenimentos';
  entretenimentos: any = [];
  entretenimento: Entretenimento;
  generosEntretenimentos: any[][] = [[]];
  idEntretenimento:  number;
  mostrarImagem = true;
  imagemLargura = 50;
  imagemMargem  = 2;
  fileNameToUpdate: string;
  modoSalvar = 'post';
  dataAtual: string;
  bodyDeletarEntretenimento: string;

  registerForm: FormGroup;

  file: File;

  constructor(private entretenimentoService: EntretenimentoService, private fb: FormBuilder,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.validation();
    this.getEntretenimentos();
  }

  openModal(template: any, idEntretenimento?: number) {
    this.registerForm.reset();
    template.show();
    this.idEntretenimento = idEntretenimento;
  }

  hideModal(template: any) {
    template.hide();
  }

  validation() {
    this.registerForm = this.fb.group({
      nome: ['', Validators.required],
      sinopse: ['', Validators.required],
      ano: ['', Validators.required],
      classificacaoEtaria: ['', Validators.required],
      duracao: ['', Validators.required],
      categoriaId: ['', Validators.required],
      urlImagem: ['']
    });
  }

  getEntretenimentos() {
    this.entretenimentoService.getEntretenimentos().subscribe(
      (entretenimentos: Entretenimento[]) => {
        this.entretenimentos = entretenimentos;

        this.entretenimentos.forEach(entretenimento => {

          const entretenimentoDto = entretenimento['entretenimentoDto'];
          const generos = entretenimento['entretenimentoDto']['generos'];

          generos.forEach(genero => {

            if (this.generosEntretenimentos[0].length > 0) {
              this.generosEntretenimentos.push([[entretenimentoDto.id, genero.nome]]);
            } else {
              this.generosEntretenimentos[0].push([entretenimentoDto.id, genero.nome]);
            }
          });
        });

        this.toastr.success('Entretenimentos buscados com sucesso!');
      },

      error => this.toastr.error('Erro ao buscar os entretenimentos!')
    );
  }

  alternarImagem() {
    this.mostrarImagem = !this.mostrarImagem;
  }

  onFileChange(event) {
    // se alguma imagem foi selecionada
    if (event.target.files && event.target.files.length) {
      this.file = event.target.files;
    }
  }

  uploadImagem() {

    if (this.modoSalvar === 'post') {

      // c:\fakepath\nome_imagem.jpg
      const nomeArquivo = this.entretenimento.urlImagem.split('\\', 3);
      this.entretenimento.urlImagem = nomeArquivo[2];

      this.entretenimentoService.postUpload(this.file, nomeArquivo[2]).subscribe(
        () => {
          /*
            passamos o tempo em milisegundos para a imagem ser trocada automaticamente sem
            precisar fazer um refresh manualmente, por causa do cache do browser
          */
          this.dataAtual = new Date().getMilliseconds().toString();
          this.getEntretenimentos();
        },

        error => this.toastr.error('Erro ao realizar o upload da imagem!')
      );

    } else {

      this.entretenimento.urlImagem = this.fileNameToUpdate;

      if (this.registerForm.get('urlImagem').value !== '') {

        this.entretenimentoService.postUpload(this.file, this.fileNameToUpdate).subscribe(
          () => {
            this.dataAtual = new Date().getMilliseconds().toString();
            this.getEntretenimentos();
          },

          error => this.toastr.error('Erro ao realizar o upload da imagem!')
        );
      }
    }
  }

  novoEntretenimento(template: any) {
    this.modoSalvar = 'post';
    this.openModal(template);
  }

  editarEntretenimento(entretenimento: Entretenimento, template: any) {
    this.modoSalvar = 'put';
    this.openModal(template);
    this.entretenimento = Object.assign({}, entretenimento);

    /*
      quando for editar um entretenimento existe a possibilidade de alterar a imagem, logo para
      evitar ter que criar um script para deletar as imagens anteriores, faremos com que o nome
      da imagem atual seja o mesmo nome da imagem anterior, para evitar ter que criar um script de
      de deleção de imagens

      o primeiro passo está em pegar o nome da imagem antes mesmo do usuário querer alterá-la
    */
    this.fileNameToUpdate = this.entretenimento.urlImagem.toString();

    /*
      na parte de criação dos dados os formControlName dos campos vão pegandos os dados que forem
      sendo passados para os inputs, textarea

      na parte de edição esses inputs, textarea vão setando os dados para os formControlName, porém
      o input do tipo file é somente get, ele não consegue setar, logo para evitar dar um erro
      dizendo urlImagem não possui valor atribuimos a ela um valor nulo
    */
    this.entretenimento.urlImagem = '';

    this.registerForm.patchValue(this.entretenimento);
  }

  excluirEntretenimento(entretenimento: Entretenimento, template: any) {
    this.openModal(template);
    this.entretenimento              = entretenimento;
    this.bodyDeletarEntretenimento = `Tem certeza que deseja excluir o entretenimento
      ${this.entretenimento.nome}?`;
  }

  confirmDelete(template: any) {
    this.entretenimentoService.deleteEntretenimento(this.entretenimento.id).subscribe(

      () => {
        this.getEntretenimentos();
        template.hide();
        this.toastr.success('Entretenimento deletado com sucesso!');
      },

      error => {
        this.toastr.error('Erro ao deletar o entretenimento');
      }
    );
  }

  salvarEntretenimento(template: any) {

    if (this.modoSalvar === 'post') {

      if (this.registerForm.valid) {
        this.entretenimento = Object.assign({}, this.registerForm.value);
        this.uploadImagem();

        this.entretenimentoService.postEntretenimento(this.entretenimento).subscribe(
          () => {
            template.hide();
            this.toastr.success('Entretenimento salvo com sucesso!');
            this.getEntretenimentos();
          },

          error => this.toastr.error('Erro ao salvar entretenimento!')
        );
      }
    } else {
      this.entretenimento = Object.assign({id: this.entretenimento.id}, this.registerForm.value);
      this.uploadImagem();

      this.entretenimentoService.putEntretenimento(this.entretenimento).subscribe(
        () => {
          template.hide();
          this.toastr.success('Entretenimento editado com sucesso!');
          this.getEntretenimentos();
        },

        error => this.toastr.error('Erro ao editar entretenimento')
      );
    }
  }
}
