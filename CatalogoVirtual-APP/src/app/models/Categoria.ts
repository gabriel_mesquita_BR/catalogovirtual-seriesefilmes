import { Entretenimento } from './Entretenimento';

export interface Categoria {

    id: number;
    nome: string;

    entretenimentos: Entretenimento[];
}
