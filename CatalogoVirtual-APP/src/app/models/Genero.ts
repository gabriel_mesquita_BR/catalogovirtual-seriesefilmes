import { Entretenimento } from './Entretenimento';

export interface Genero {

    id: number;
    nome: string;

    entretenimentos: Entretenimento[];
}
