import { Categoria } from './Categoria';
import { Genero } from './Genero';

export interface Entretenimento {
    id: number;
    nome: string;
    sinopse: string;
    ano: string;
    classificacaoEtaria: number;
    duracao: string;
    urlImagem: string;
    idCategoria: number;

    categoria: Categoria;
    generos: Genero[];
}
