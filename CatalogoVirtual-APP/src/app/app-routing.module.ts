import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EntretenimentoComponent } from './components/entretenimento/entretenimento.component';
import { UserComponent } from './components/user/user.component';
import { LoginComponent } from './components/user/login/login.component';
import { RegisterComponent } from './components/user/register/register.component';
import { AuthGuard } from './auth/auth.guard';
import { EntretenimentoGeneroComponent } from './components/entretenimentoGenero/entretenimentoGenero.component';
import { IndexComponent } from './components/index/index.component';

const routes: Routes = [

  {path: 'user', component: UserComponent, children: [
    {path: 'login', component: LoginComponent},
    {path: 'register', component: RegisterComponent}
  ]},

  {path: 'entretenimentos', component: EntretenimentoComponent, canActivate: [AuthGuard]},
  {path: 'entretenimentosGeneros', component: EntretenimentoGeneroComponent, canActivate: [AuthGuard]},
  {path: 'index', component: IndexComponent},

  {path: '', redirectTo: 'index', pathMatch: 'full'},
  {path: '**', redirectTo: 'index', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
