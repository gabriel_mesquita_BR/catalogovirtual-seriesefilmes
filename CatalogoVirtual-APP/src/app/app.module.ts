import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EntretenimentoComponent } from './components/entretenimento/entretenimento.component';
import { TituloComponent } from './components/titulo/titulo.component';

import { ModalModule, TooltipModule } from 'ngx-bootstrap';

import {HttpClientModule} from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './components/nav/nav.component';
import { UserComponent } from './components/user/user.component';
import { LoginComponent } from './components/user/login/login.component';
import { RegisterComponent } from './components/user/register/register.component';
import { EntretenimentoGeneroComponent } from './components/entretenimentoGenero/entretenimentoGenero.component';

import { NgSelectModule } from '@ng-select/ng-select';

import { ToastrModule } from 'ngx-toastr';
import { IndexComponent } from './components/index/index.component';

@NgModule({
   declarations: [
      AppComponent,
      EntretenimentoComponent,
      TituloComponent,
      NavComponent,
      UserComponent,
      LoginComponent,
      RegisterComponent,
      EntretenimentoGeneroComponent,
      IndexComponent
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      ModalModule.forRoot(),
      TooltipModule.forRoot(),
      HttpClientModule,
      ReactiveFormsModule,
      BrowserAnimationsModule,
      FormsModule,
      NgSelectModule,
      ToastrModule.forRoot({
         timeOut: 3000,
         preventDuplicates: true,
      }),
   ],

   providers: [],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
