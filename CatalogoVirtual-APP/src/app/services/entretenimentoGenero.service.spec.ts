/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { EntretenimentoGeneroService } from './entretenimentoGenero.service';

describe('Service: EntretenimentoGenero', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EntretenimentoGeneroService]
    });
  });

  it('should ...', inject([EntretenimentoGeneroService], (service: EntretenimentoGeneroService) => {
    expect(service).toBeTruthy();
  }));
});
