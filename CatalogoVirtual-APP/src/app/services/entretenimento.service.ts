import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Entretenimento } from 'src/app/models/Entretenimento';

@Injectable({
  providedIn: 'root'
})
export class EntretenimentoService {

  baseUrl = 'http://localhost:5000/api/entretenimento';

  constructor(private http: HttpClient) { }

  getEntretenimentos() {
    return this.http.get(this.baseUrl);
  }

  postUpload(file: File, nomeArquivo: string) {
    const fileUpload = file[0] as File;
    const formData   = new FormData();

    formData.append('file', fileUpload, nomeArquivo);
    return this.http.post(`${this.baseUrl}/upload`, formData);
  }

  postEntretenimento(entretenimento: Entretenimento) {
    return this.http.post(this.baseUrl, entretenimento);
  }

  putEntretenimento(entretenimento: Entretenimento) {
    return this.http.put(`${this.baseUrl}/${entretenimento.id}`, entretenimento);
  }

  deleteEntretenimento(idEntretenimento: number) {
    return this.http.delete(`${this.baseUrl}/${idEntretenimento}`);
  }
}
