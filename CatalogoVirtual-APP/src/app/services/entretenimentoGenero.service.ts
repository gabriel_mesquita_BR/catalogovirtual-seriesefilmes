import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EntretenimentoGeneroService {

  baseUrl = 'http://localhost:5000/api/entretenimentogenero';

  constructor(private http: HttpClient) { }

  getEntretenimentosGeneros() {
    return this.http.get(this.baseUrl);
  }

  postEntretenimentoGenero(entretenimentoGenero: any) {
    return this.http.post(this.baseUrl, entretenimentoGenero);
  }
}
