/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { EntretenimentoService } from './entretenimento.service';

describe('Service: Entretenimento', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EntretenimentoService]
    });
  });

  it('should ...', inject([EntretenimentoService], (service: EntretenimentoService) => {
    expect(service).toBeTruthy();
  }));
});
