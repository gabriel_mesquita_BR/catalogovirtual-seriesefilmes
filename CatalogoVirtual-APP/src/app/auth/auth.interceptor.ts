import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
// se não importar todo o caminho de tap, surgirá problemas
import { tap } from 'rxjs/internal/operators/tap';

@Injectable({
    providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {

    constructor(private router: Router) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        if (localStorage.getItem('token') !== null) {

            const cloneReq = req.clone({
                headers: req.headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`)
            });

            return next.handle(cloneReq).pipe(
                tap(
                    success => {},
                    error => {
                        if (error.status === 401) {
                            this.router.navigate(['/user/login']);
                        }
                    }
                )
            );
        } else {
            /*
                se o usuário não estiver logado, libero a requisição sem fazer alterações em
                seu cabeçalho
            */
            return next.handle(req.clone());
        }
    }
}
