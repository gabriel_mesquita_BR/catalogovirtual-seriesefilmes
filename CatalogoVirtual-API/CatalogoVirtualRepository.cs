using System.Runtime.InteropServices;
using System;
using System.Linq;
using System.Threading.Tasks;
using CatalogoVirtual_Api.Database;
using CatalogoVirtual_Api.Interface;
using CatalogoVirtual_Api.Models;
using Microsoft.EntityFrameworkCore;

using System.Collections.Generic;

namespace CatalogoVirtual_API
{
    public class CatalogoVirtualRepository: ICatalogoVirtualRepository
    {
        private ApplicationDbContext _context;
        public CatalogoVirtualRepository(ApplicationDbContext context)
        {
            this._context = context;

            /*
                forma geral de remover tracking
                evitando que o entity framework trave a aplicação após uma busca
            */

            this._context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        public void Add<T>(T entity) where T : class
        {
            this._context.Add(entity);
        }

        public void Update<T>(T entity) where T : class
        {
            this._context.Update(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            this._context.Remove(entity);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return (await this._context.SaveChangesAsync()) > 0;
        }

        public async Task<Entretenimento[]> GetEntretenimentos()
        {
            // Include = Join
            IQueryable<Entretenimento> query = this._context.Entretenimentos
                .Include(categoria  => categoria.Categoria)
                .Include(eg         => eg.EntretenimentosGeneros)
                .ThenInclude(genero => genero.Genero);

            query = query.OrderBy(entretenimento => entretenimento.Id);

            return await query.ToArrayAsync();
        }

        public async Task<Entretenimento[]> GetEntretenimentosByCategoria(int categoria)
        {
            IQueryable<Entretenimento> query = this._context.Entretenimentos
                .Include(cat        => cat.Categoria)
                .Include(eg         => eg.EntretenimentosGeneros)
                .ThenInclude(genero => genero.Genero);

            query = query.OrderBy(entretenimento => entretenimento.Categoria.Nome)
                .Where(entretenimento => entretenimento.CategoriaId == categoria);
            
            return await query.ToArrayAsync();
        }

        public async Task<Entretenimento[]> GetEntretenimentosByGenero(int genero)
        {
            // retorna um array de ids da chave estrangeira generoId
            Task<int[]> idsEntretenimentos = this._context.EntretenimentosGeneros.Where(eg => eg.GeneroId == genero)
                .Select(column => column.EntretenimentoId).ToArrayAsync();

            int[] ids = idsEntretenimentos.Result;

            IQueryable<Entretenimento> query = this._context.Entretenimentos
                .Include(categoria => categoria.Categoria)
                .Include(eg        => eg.EntretenimentosGeneros)
                .ThenInclude(g     => g.Genero);

            query = query.OrderBy(e => e.Id).Where(e => ids.Contains(e.Id));

            return await query.ToArrayAsync();
        }

        public async Task<Entretenimento> GetEntretenimentoById(int id)
        {
            IQueryable<Entretenimento> query = this._context.Entretenimentos
                .Include(categoria => categoria.Categoria)
                .Include(eg => eg.EntretenimentosGeneros)
                .ThenInclude(genero => genero.Genero);

            query = query.Where(entretenimento => entretenimento.Id == id);

            return await query.FirstOrDefaultAsync();
        }

        // public async Task<Entretenimento> GetLastEntretenimento()
        // {
        //     IQueryable<Entretenimento> query = this._context.Entretenimentos
        //         .Include(categoria  => categoria.Categoria)
        //         .Include(eg         => eg.EntretenimentosGeneros)
        //         .ThenInclude(genero => genero.Genero);

        //     int lastId = this._context.Entretenimentos.Max(e => e.Id);
            
        //     return await query.FirstOrDefaultAsync(e => e.Id == lastId);
        // }

        public async Task<EntretenimentoGenero[]> GetEntretenimentosGenerosRelacionados()
        {
            IQueryable<EntretenimentoGenero> query = this._context.EntretenimentosGeneros
                .Include(eg => eg.Entretenimento)
                .Include(eg => eg.Genero);

            query.Select(eg => new { 
                entretenimentoId = eg.Entretenimento.Id, entretenimentoNome = eg.Entretenimento.Nome,
                generoId = eg.Genero.Id, generoNome = eg.Genero.Nome
            });

            return await query.ToArrayAsync();
        }

        // pega os gêneros relacionados ao entretenimento selecionado
        public async Task<EntretenimentoGenero[]> GetGenerosFromEntretenimentos(int idEntretenimento)
        {
            IQueryable<EntretenimentoGenero> query = this._context.EntretenimentosGeneros;
            query = query.Where(eg => eg.EntretenimentoId == idEntretenimento); 
            return await query.ToArrayAsync();
        }

        public async Task<EntretenimentoGenero> GetEntretenimentoGenero(int idEntretenimento, int idGenero)
        {
            IQueryable<EntretenimentoGenero> query = this._context.EntretenimentosGeneros;
            query = query.Where(eg => eg.EntretenimentoId == idEntretenimento && 
                eg.GeneroId == idGenero);
            return await query.FirstOrDefaultAsync();
        }

        public async Task<EntretenimentoGenero> GetEntretenimentoGeneroFromEntretenimento(int idEntretenimento)
        {
            IQueryable<EntretenimentoGenero> query = this._context.EntretenimentosGeneros;
            query = query.Where(eg => eg.EntretenimentoId == idEntretenimento);
            return await query.FirstOrDefaultAsync();
        }

        public async Task<Categoria[]> GetCategorias()
        {
            IQueryable<Categoria> query = this._context.Categorias;
            return await query.ToArrayAsync();
        }

        public async Task<Genero[]> GetGeneros()
        {
            IQueryable<Genero> query = this._context.Generos;
            return await query.ToArrayAsync();
        }
    }
}