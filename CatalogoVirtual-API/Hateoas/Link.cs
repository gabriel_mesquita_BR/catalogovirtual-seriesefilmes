namespace CatalogoVirtual_API.Hateoas
{
    public class Link
    {
        public string Href {get; set;} // link
        public string Rel {get; set;} // descrição do link
        public string Method {get; set;} // método do link

        public Link(string href, string rel, string method)
        {
            this.Href   = href;
            this.Rel    = rel;
            this.Method = method;
        }
    }
}