using System.Collections.Generic;

namespace CatalogoVirtual_API.Hateoas
{
    public class Hateoas
    {
        private readonly string url;
        private readonly string protocol = "http://";

        // lista de links são as actions dos controllers (rotas)
        public List<Link> actions = new List<Link>();

        public Hateoas(string url)
        {
            this.url = url;
        }

        // adiciono os links, as actions, as rotas
        public void AddAction(string rel, string method)
        {
            actions.Add(new Link(this.protocol + this.url, rel, method));
        }

        public Link[] GetActions(string sufixo) // sufixo eh o id
        {
            Link[] links = new Link[this.actions.Count];

            for(int i = 0; i < links.Length; i++)
            {
                links[i] = new Link(this.actions[i].Href, this.actions[i].Rel, this.actions[i].Method);
            }

            foreach(var link in links)
            {
                link.Href = link.Href + "/" + sufixo;
            }

            return links;
        }
    }
}