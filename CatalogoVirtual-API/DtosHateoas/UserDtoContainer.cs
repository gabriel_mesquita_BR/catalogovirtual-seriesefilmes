using CatalogoVirtual_API.Dtos;
using CatalogoVirtual_API.Hateoas;

namespace CatalogoVirtual_API.DtosHateoas
{
    public class UserDtoContainer
    {
        public UserDto UserDto {get; set;}
        public Link[] Links {get; set;}
    }
}