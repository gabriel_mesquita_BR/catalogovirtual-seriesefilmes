using CatalogoVirtual_Api.Dtos;
using CatalogoVirtual_API.Hateoas;

namespace CatalogoVirtual_API.DtosHateoas
{
    public class EntretenimentoDtoContainer
    {
        public EntretenimentoDto EntretenimentoDto{get; set;}
        public Link[] Links {get; set;}
    }
}