using CatalogoVirtual_API.Dtos;
using CatalogoVirtual_API.Hateoas;

namespace CatalogoVirtual_API.DtosHateoas
{
    public class EntretenimentoGeneroIdNomeDtoContainer
    {
        public EntretenimentoGeneroIdNomeDto EntretenimentoGeneroIdNomeDto {get; set;}
        public Link[] Links {get; set;}
    }
}