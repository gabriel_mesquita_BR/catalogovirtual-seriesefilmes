using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using CatalogoVirtual_API.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace CatalogoVirtual_API
{
    public class JWToken
    {
        public static string Generate(User user, IConfiguration config)
        {
            var claims = new List<Claim> {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.Role, "Admin")
            };

            // chave secreta
            var secretKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(
                config.GetSection("AppSettings:Token").Value
            ));

            // algoritmo de encodamento
            var credentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha512Signature);

            // payload do jwt
            var tokenDescriptor = new SecurityTokenDescriptor{
                // a quem pertence este token, no caso será ao usuário logado
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = credentials,
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token        = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}