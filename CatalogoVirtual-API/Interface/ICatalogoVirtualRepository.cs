using System.Threading.Tasks;
using CatalogoVirtual_Api.Models;

namespace CatalogoVirtual_Api.Interface
{
    public interface ICatalogoVirtualRepository
    {
        void Add<T>(T entity) where T: class;
        void Update<T>(T entity) where T: class;
        void Delete<T>(T entity) where T: class;

        Task<bool> SaveChangesAsync();

        Task<Entretenimento[]> GetEntretenimentos();
        Task<Entretenimento[]> GetEntretenimentosByCategoria(int categoria);
        Task<Entretenimento[]> GetEntretenimentosByGenero(int genero);
        Task<Entretenimento> GetEntretenimentoById(int id);

        // Task<Entretenimento> GetLastEntretenimento();

        Task<EntretenimentoGenero[]> GetEntretenimentosGenerosRelacionados();

        Task<EntretenimentoGenero[]> GetGenerosFromEntretenimentos(int idEntretenimento);

        Task<EntretenimentoGenero> GetEntretenimentoGenero(int idEntretenimento, int idGenero);

        Task<Categoria[]> GetCategorias();

        Task<EntretenimentoGenero> GetEntretenimentoGeneroFromEntretenimento(int idEntretenimento);
        
        Task<Genero[]> GetGeneros();
    }
}