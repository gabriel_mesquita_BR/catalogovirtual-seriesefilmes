using System;
using CatalogoVirtual_Api.Models;
using CatalogoVirtual_API.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace CatalogoVirtual_Api.Database
{
    // public class ApplicationDbContext : DbContext
    public class ApplicationDbContext : IdentityDbContext<User, Role, int, IdentityUserClaim<int>,
                                                          UserRole, IdentityUserLogin<int>,
                                                          IdentityRoleClaim<int>, IdentityUserToken<int>>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) {}

        public DbSet<Entretenimento> Entretenimentos {get; set;}
        public DbSet<Genero> Generos {get;set;}
        public DbSet<EntretenimentoGenero> EntretenimentosGeneros {get;set;}
        public DbSet<Categoria> Categorias {get; set;}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // processo para criação das tabelas e relacionamento do Identity
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<UserRole>(userRole => {
                userRole.HasKey(ur => new {ur.UserId, ur.RoleId});

                userRole.HasOne(ur => ur.Role)
                    .WithMany(u => u.UsersRoles)
                    .HasForeignKey(r => r.RoleId)
                    .IsRequired();

                userRole.HasOne(ur => ur.User)
                    .WithMany(r => r.UsersRoles)
                    .HasForeignKey(u => u.UserId)
                    .IsRequired();
            });

            // ==================================================================== //

            // confirma o relacionamento N por N entre Entretenimento e Genero
            modelBuilder.Entity<EntretenimentoGenero>()
                .HasKey(EG => new {EG.EntretenimentoId, EG.GeneroId});

            modelBuilder.Entity<EntretenimentoGenero>()
                .HasOne(e        => e.Entretenimento)
                .WithMany(eg     => eg.EntretenimentosGeneros)
                .HasForeignKey(e => e.EntretenimentoId);

            modelBuilder.Entity<EntretenimentoGenero>()
                .HasOne(g        => g.Genero)
                .WithMany(e      => e.EntretenimentosGeneros)
                .HasForeignKey(g => g.GeneroId);
        }
    }
}