using System.Linq;
using AutoMapper;
using CatalogoVirtual_Api.Dtos;
using CatalogoVirtual_Api.Models;
using CatalogoVirtual_API.Dtos;
using CatalogoVirtual_API.Identity;

namespace CatalogoVirtual_Api.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<Entretenimento, EntretenimentoDto>()
                .ForMember(dest => dest.Generos, opt => {
                    opt.MapFrom(src => src.EntretenimentosGeneros.Select(x => x.Genero).ToList());
                }).ReverseMap();

            CreateMap<Entretenimento, EntretenimentoIdNomeDto>().ReverseMap();
            
            CreateMap<Genero, GeneroDto>()
                .ForMember(dest => dest.Entretenimentos, opt => {
                    opt.MapFrom(src => src.EntretenimentosGeneros.Select(x => x.Entretenimento).ToList());
                }).ReverseMap();

            // não quero trazer os dados de entretenimento dentro de gêneros, por isso não uso o ForMember
            CreateMap<Genero, GeneroIdNomeDto>().ReverseMap();

            CreateMap<Categoria, CategoriaDto>().ReverseMap();

            CreateMap<EntretenimentoGenero, EntretenimentoGeneroDto>().ReverseMap();

            CreateMap<User, UserDto>().ReverseMap();

            CreateMap<User, UserLoginDto>().ReverseMap();

            CreateMap<EntretenimentoGenero, EntretenimentoGeneroIdNomeDto>().ReverseMap();
        }
    }
}