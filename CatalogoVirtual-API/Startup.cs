﻿using System.IO;
using System.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using CatalogoVirtual_Api.Database;
using CatalogoVirtual_Api.Interface;
using CatalogoVirtual_Api.Models;
using Microsoft.EntityFrameworkCore;

using AutoMapper;
using Microsoft.AspNetCore.Identity;
using CatalogoVirtual_API.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.Http;

namespace CatalogoVirtual_API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options => {    

                /*
                    qualquer rota que for acessada é preciso ter autorização, é preciso passar
                    no cabeçalho da requisição o jwt

                    para burlar essa política pode-se usar nas rotas desejadas a data annotation
                    chamada AllowAnonymous
                */

                var policy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build();
                options.Filters.Add(new AuthorizeFilter(policy));

            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
              .AddJsonOptions(opt =>
                {
                    // evita o loop entre as entidades relacionadas
                    opt.SerializerSettings.ReferenceLoopHandling =
                        Newtonsoft.Json.ReferenceLoopHandling.Ignore;        
                });

            services.AddDbContext<ApplicationDbContext>(options => 
                options.UseMySql(Configuration.GetConnectionString("DefaultConnection"))
            );

            // uma forma de autorização simples
            // services.AddAuthorization(options => options.AddPolicy("TemNome", policy =>
            //     policy.RequireClaim("FullName", "Gabriel")));

            // configurando Identity

            IdentityBuilder builder = services.AddIdentityCore<User>(options => {

                options.Password.RequireDigit           = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireLowercase       = false;
                options.Password.RequireUppercase       = false;
                options.Password.RequiredLength         = 6;
            });

            /*
                builder.UserType é do tipo Role, pois User relaciona-se com Role
                builder.Services foi criado acima
            */

            builder = new IdentityBuilder(builder.UserType, typeof(Role), builder.Services);
            builder.AddEntityFrameworkStores<ApplicationDbContext>();
            builder.AddRoleValidator<RoleValidator<Role>>();
            builder.AddRoleManager<RoleManager<Role>>();
            builder.AddSignInManager<SignInManager<User>>();

            // configurando JWT
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options => {
                    
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey      = true,
                        IssuerSigningKey              = new SymmetricSecurityKey(Encoding
                            .ASCII.GetBytes(Configuration.GetSection("AppSettings:Token").Value)),
                        ValidateIssuer                = false,
                        ValidateAudience              = false
                    };
                });

            /*
                quando a interface ICatalogoVirtualRepository for injetada no controller, a classe
                CatalogoVirtualRepository que extende da interface ICatalogoVirtualRepository será
                executada
            */
            
            services.AddScoped<ICatalogoVirtualRepository, CatalogoVirtualRepository>();

            services.AddAutoMapper(typeof(Startup));

            services.AddCors();

            services.AddSwaggerGen(config => config.SwaggerDoc("v1", 
                new Microsoft.OpenApi.Models.OpenApiInfo {
                    Title="API DE CATÁLOGO VIRTUAL - SÉRIES E FILMES", Version="v1"}));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseAuthentication();

            // app.UseHttpsRedirection();
            
            app.UseCors(cors => cors.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());

            /*
                para trabalhar com imagens, sendo que estas imagens serão armazenadas no diretório
                wwwroot, basta colocar o comando abaixo
            */
            // app.UseStaticFiles();

            // as imagens serão em armazenadas no diretório Resources
            app.UseStaticFiles(new StaticFileOptions() {

                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(),
                    "Resources")),

                RequestPath = new PathString("/Resources")
            });

            // app.UseSwagger();
            // esse é o endpoint padrão do swagger
            // app.UseSwaggerUI(config => config.SwaggerEndpoint("/swagger/v1/swagger.json", "v1 docs"));

            // para mudarmos o endpoint, temos que mudar o diretório de armazenamento do swagger
            // {documentName} pega a versão da documentação, do swagger
            app.UseSwagger(config => config.RouteTemplate = 
                "catalogovirtual-seriesefilmes/{documentName}/swagger.json");

            app.UseSwaggerUI(config => config.SwaggerEndpoint(
                "/catalogovirtual-seriesefilmes/v1/swagger.json", "v1 docs"));

            app.UseMvc();
        }
    }
}
