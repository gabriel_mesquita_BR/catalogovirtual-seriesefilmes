using System.Collections.Generic;

namespace CatalogoVirtual_Api.Models
{
    public class Entretenimento
    {
        public int Id {get; set;}
        public string Nome {get; set;}
        public string sinopse {get; set;}
        public int Ano {get; set;}
        public int ClassificacaoEtaria {get; set;}
        public string Duracao {get; set;}
        public string UrlImagem {get; set;}
        public int CategoriaId {get; set;}
        // public int GeneroId {get; set;}
        public Categoria Categoria {get; set;} // série ou filme
        public List<EntretenimentoGenero> EntretenimentosGeneros {get; set;}

        // public List<Genero> Generos {get; set;} // ação, comédia,...
    }
}