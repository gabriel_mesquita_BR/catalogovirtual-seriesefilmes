namespace CatalogoVirtual_Api.Models
{
    // Entretenimento e Genero => multiplicidade N por N
    public class EntretenimentoGenero
    {
        public int EntretenimentoId {get; set;}
        public int GeneroId {get; set;}

        public Entretenimento Entretenimento {get; set;}
        public Genero Genero {get; set;}
    }
}