using System.Collections.Generic;

namespace CatalogoVirtual_Api.Models
{
    public class Categoria
    {
        public int Id {get; set;}
        public string Nome {get; set;}
        public List<Entretenimento> Entretenimentos {get; set;}
    }
}