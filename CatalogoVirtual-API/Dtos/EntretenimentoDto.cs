using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CatalogoVirtual_Api.Dtos
{
    public class EntretenimentoDto
    {
        public int Id {get; set;}

        [Required(ErrorMessage="O nome é obrigatório")]
        public string Nome {get; set;}

        [Required(ErrorMessage="A sinopse é obrigatório")]
        public string sinopse {get; set;}

        [Required(ErrorMessage="O ano é obrigatório")]
        public int Ano {get; set;}

        [Required(ErrorMessage="A classificação etária é obrigatório")]
        public int ClassificacaoEtaria {get; set;}

        [Required(ErrorMessage="A duração é obrigatório")]
        public string Duracao {get; set;}

        // [Required(ErrorMessage="A imagem é obrigatório")]
        public string UrlImagem {get; set;}

        [Required(ErrorMessage="A categoria é obrigatório")]
        public int CategoriaId {get; set;}

        // [Required(ErrorMessage="O gênero é obrigatório")]
        // public int GeneroId {get; set;}

        public CategoriaDto Categoria {get; set;} // série ou filme
        public List<GeneroIdNomeDto> Generos {get; set;}
    }
}