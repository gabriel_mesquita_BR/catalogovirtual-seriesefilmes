namespace CatalogoVirtual_API.Dtos
{
    public class EntretenimentoGeneroIdNomeDto
    {
        public int EntretenimentoId {get; set;}
        public string EntretenimentoNome {get; set;}
        public int GeneroId {get; set;}
        public string GeneroNome {get; set;}
    }
}