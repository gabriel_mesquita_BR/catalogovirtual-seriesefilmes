using System.Collections.Generic;

namespace CatalogoVirtual_Api.Dtos
{
    public class GeneroDto
    {
        public int Id {get; set;}
        public string Nome {get; set;}
        public List<EntretenimentoDto> Entretenimentos {get; set;}
    }
}