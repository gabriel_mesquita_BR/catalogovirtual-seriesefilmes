using System.ComponentModel.DataAnnotations;

namespace CatalogoVirtual_Api.Dtos
{
    public class EntretenimentoIdNomeDto
    {
        public int Id {get; set;}

        [Required(ErrorMessage="O nome é obrigatório")]
        public string Nome {get; set;}
    }
}