using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace CatalogoVirtual_Api.Dtos
{
    public class GeneroIdNomeDto
    {
        public int Id {get; set;}
        
        [Required(ErrorMessage="O nome gênero é obrigatório")]
        public string Nome {get; set;}

        // public List<EntretenimentoDto> Entretenimentos {get; set;}
    }
}