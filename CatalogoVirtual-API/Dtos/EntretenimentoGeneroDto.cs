namespace CatalogoVirtual_Api.Dtos
{
    public class EntretenimentoGeneroDto
    {
        public int EntretenimentoId {get; set;}
        public int GeneroId {get; set;}

        // public EntretenimentoDto Entretenimento {get; set;}
        // public GeneroDto Genero {get; set;}
    }
}