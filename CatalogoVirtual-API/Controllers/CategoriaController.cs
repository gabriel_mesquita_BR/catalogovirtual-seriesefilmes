using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CatalogoVirtual_Api.Dtos;
using CatalogoVirtual_Api.Interface;
using CatalogoVirtual_API.DtosHateoas;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CatalogoVirtual_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriaController : ControllerBase
    {
        // Não há necessidade de usar Hateoas neste controller
        
        private readonly ICatalogoVirtualRepository _context;
        private readonly IMapper _mapper;
        public CategoriaController(ICatalogoVirtualRepository context, IMapper mapper)
        {
            this._context = context;
            this._mapper  = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetCategorias()
        {
            try 
            {
                var categorias = await this._context.GetCategorias();
                var results    = this._mapper.Map<CategoriaDto[]>(categorias);
                return Ok(results);

            }catch(Exception exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                    $"Banco de dados falhou {exception.Message}");
            }
        }
    }
}