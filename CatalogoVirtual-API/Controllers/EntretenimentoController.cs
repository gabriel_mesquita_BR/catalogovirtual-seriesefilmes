using System.Net.Http.Headers;
using System;
using System.Threading.Tasks;
using CatalogoVirtual_Api.Database;
using CatalogoVirtual_Api.Models;
using CatalogoVirtual_Api.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using System.Collections.Generic;
using CatalogoVirtual_Api.Dtos;
using System.IO;
using CatalogoVirtual_API.DtosHateoas;
using CatalogoVirtual_API.Hateoas;
using Microsoft.AspNetCore.Authorization;

namespace CatalogoVirtual_Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EntretenimentoController : ControllerBase
    {
        // private ApplicationDbContext _context;
        
        // public EntretenimentoController(ApplicationDbContext context) 
        // {
        //     this._context = context;
        // }

        private ICatalogoVirtualRepository _context;
        private readonly IMapper _mapper;

        private readonly Hateoas _hateoas;

        public EntretenimentoController(ICatalogoVirtualRepository context, IMapper mapper) 
        {
            this._context = context;
            this._mapper  = mapper;

            // Hateoas
            this._hateoas = new Hateoas("localhost:5000/api/entretenimento");
            this._hateoas.AddAction("GET_ENTRETENIMENTO", "GET");
            this._hateoas.AddAction("GET_ENTRETENIMENTO_By_Categoria", "GET");
            this._hateoas.AddAction("PUT_ENTRETENIMENTO", "PUT");
            this._hateoas.AddAction("DELETE_ENTRETENIMENTO", "DELETE");
        }

        // pega todos os entretenimentos

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try 
            {
                // retorna um domínio
                var entretenimentos = await this._context.GetEntretenimentos();
                 // mapeamento entre domínio e dto
                var results         = this._mapper.Map<IEnumerable<EntretenimentoDto>>(entretenimentos); 

                List<EntretenimentoDtoContainer> entretenimentosHateoas = 
                    new List<EntretenimentoDtoContainer>();

                foreach(var result in results)
                {
                    EntretenimentoDtoContainer entretenimentoHateoas = new EntretenimentoDtoContainer();
                    entretenimentoHateoas.EntretenimentoDto          = result;
                    entretenimentoHateoas.Links                      = 
                        this._hateoas.GetActions(result.Id.ToString());
                    
                    entretenimentosHateoas.Add(entretenimentoHateoas);
                }

                return Ok(entretenimentosHateoas);
                // return Ok(results);

            }catch(Exception exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                    $"Banco de dados falhou {exception.Message}");
            }
        }

        // pega um entretenimento

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try 
            {
                var entretenimento = await this._context.GetEntretenimentoById(id);
                var result         = this._mapper.Map<EntretenimentoDto>(entretenimento);
                
                EntretenimentoDtoContainer entretenimentoHateoas = new EntretenimentoDtoContainer();
                entretenimentoHateoas.EntretenimentoDto          = result;
                entretenimentoHateoas.Links                      = 
                    this._hateoas.GetActions(result.Id.ToString());

                return Ok(entretenimentoHateoas);
                
                // return Ok(result);
            }catch(Exception exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                    $"Banco de dados falhou {exception.Message}");
            }
        }

        // pega os entretenimentos por categoria 

        [HttpGet("getByCategoria/{categoria}")]
        public async Task<IActionResult> GetByCategoria(int categoria)
        {
            try 
            {
                var entretenimentos = await this._context.GetEntretenimentosByCategoria(categoria);
                var results         = this._mapper.Map<IEnumerable<EntretenimentoDto>>(entretenimentos);
                
                List<EntretenimentoDtoContainer> entretenimentosHateoas = 
                    new List<EntretenimentoDtoContainer>();

                foreach(var result in results)
                {
                    EntretenimentoDtoContainer entretenimentoHateoas = new EntretenimentoDtoContainer();
                    entretenimentoHateoas.EntretenimentoDto          = result;
                    entretenimentoHateoas.Links                      = 
                        this._hateoas.GetActions(result.Id.ToString());

                    entretenimentosHateoas.Add(entretenimentoHateoas);
                }

                return Ok(entretenimentosHateoas);

                // return Ok(results);
            }catch(Exception exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                    $"Banco de dados falhou {exception.Message}");
            }
        }

        // pega os entretenimentos por gênero

        [HttpGet("getByGenero/{genero}")]
        public async Task<IActionResult> GetByGenero(int genero)
        {
            try 
            {
                var entretenimentos = await this._context.GetEntretenimentosByGenero(genero);
                var results         = this._mapper.Map<IEnumerable<EntretenimentoDto>>(entretenimentos);
                
                List<EntretenimentoDtoContainer> entretenimentosHateoas = 
                    new List<EntretenimentoDtoContainer>();

                foreach(var result in results)
                {
                    EntretenimentoDtoContainer entretenimentoHateoas = new EntretenimentoDtoContainer();
                    entretenimentoHateoas.EntretenimentoDto          = result;
                    entretenimentoHateoas.Links                      = 
                        this._hateoas.GetActions(result.Id.ToString());
                    
                    entretenimentosHateoas.Add(entretenimentoHateoas);
                }

                return Ok(entretenimentosHateoas);
            }catch(Exception exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                    $"Banco de dados falhou {exception.Message}");
            }
        }

        // insere um entretenimento
        // [Authorize("TemNome")]
        [HttpPost]
        public async Task<IActionResult> Post(Entretenimento model)
        {
            try 
            {
                 // mapeamento reverso (dto e domínio)
                var entretenimentos = this._mapper.Map<Entretenimento>(model);
                this._context.Add(model);

                if (await this._context.SaveChangesAsync())
                    return Created("/api/entretenimento", this._mapper.Map<EntretenimentoDto>(entretenimentos));
               
            }catch (Exception exception) {

                return this.StatusCode(StatusCodes.Status500InternalServerError, 
                    $"Banco de dados falhou {exception.Message}");
            }

            return BadRequest();
        }

        // atualiza um entretenimento

        [HttpPut("{entretenimentoId}")]
        public async Task<IActionResult> Put(int entretenimentoId, EntretenimentoDto model)
        {
            try
            {
                var entretenimento = await this._context.GetEntretenimentoById(entretenimentoId);

                if(entretenimento == null) return NotFound();

                this._mapper.Map(model, entretenimento);
                this._context.Update(entretenimento);

                if(await this._context.SaveChangesAsync())
                    return Created("/api/entretenimento", this._mapper.Map<EntretenimentoDto>(entretenimento));

            }catch(Exception exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                    $"Banco de dados falhou {exception.Message}");
            }

            return BadRequest();
        }

        // deleta um entretenimento

        [HttpDelete("{entretenimentoId}")]
        public async Task<IActionResult> Delete(int entretenimentoId)
        {
            try
            {
                var entretenimentoGeneros = 
                    await this._context.GetEntretenimentoGeneroFromEntretenimento(entretenimentoId);
                
                if(entretenimentoGeneros == null) return NotFound();

                // deleto o relacionamento entre entretenimento e gênero    
                this._context.Delete(entretenimentoGeneros);

                if (await this._context.SaveChangesAsync()) 
                {
                    var entretenimento = await this._context.GetEntretenimentoById(entretenimentoId);

                    if (entretenimento == null) return NotFound();

                    // deleto o entretenimento
                    this._context.Delete(entretenimento);
        
                    if (await this._context.SaveChangesAsync())
                        return Ok();
                }

            }catch(Exception exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                    $"Banco de dados falhou {exception.Message}");
            }

            return BadRequest();
        }

        [HttpPost("upload")]
        public IActionResult Upload()
        {
            try 
            {
                // pega o arquivo que vem no formato de array
                var file = Request.Form.Files[0]; 

                // combino o caminho onde as imagens serão salvas
                var folderName = Path.Combine("Resources", "Images");   

                // combino o caminho do diretório atual com o diretório aonde as imagens serão salvas
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

                if(file.Length > 0) {
                    
                    // pego o nome do arquivo
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition)
                        .FileName;

                    /*
                        combino o diretório que as imagens serão salvas com o nome do arquivo
                        se o nome do arquivo tiver aspas duplas, substituo por string vazia
                        se o nome do arquivo tiver espações, removo esses espaços
                    */
                    var fullPath = Path.Combine(pathToSave, fileName.Replace("\"", "").Trim());

                    // criar a imagem no caminho definido
                    using(var stream = new FileStream(fullPath, FileMode.Create))
                        file.CopyTo(stream);
                }
                
                return Ok();

            }catch(Exception exception)
            {
                this.StatusCode(StatusCodes.Status500InternalServerError,
                    $"Banco de dados falhou {exception}");
            }

            return BadRequest();
        }

    }
}