using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using System;
using System.Threading.Tasks;
using CatalogoVirtual_Api.Interface;
using CatalogoVirtual_Api.Models;
using CatalogoVirtual_Api.Dtos;
using System.Collections.Generic;
using CatalogoVirtual_API.DtosHateoas;
using Microsoft.AspNetCore.Authorization;
using CatalogoVirtual_API.Dtos;
using CatalogoVirtual_API.Hateoas;

namespace CatalogoVirtual_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EntretenimentoGeneroController: ControllerBase
    {
        private readonly ICatalogoVirtualRepository _context;
        private readonly IMapper _mapper;

        private readonly Hateoas.Hateoas _hateoas;

        public EntretenimentoGeneroController(ICatalogoVirtualRepository context, IMapper mapper) 
        {
            this._context = context;
            this._mapper  = mapper;

            this._hateoas = new Hateoas.Hateoas("localhost:5000/api/entretenimentogenero");
            this._hateoas.AddAction("GET_ENTRETENIMENTOGENERO", "GET");
            this._hateoas.AddAction("DELETE_ENTRETENIMENTOGENERO", "DELETE");
        }

        // pega todos os entretenimentos (id, nome) e todos gêneros (id, nome)

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try 
            {
                var entretenimentos       = await this._context.GetEntretenimentos();
                var generos               = await this._context.GetGeneros();

                var entretenimentosDto    = 
                    this._mapper.Map<EntretenimentoIdNomeDto[]>(entretenimentos);

                var generosDto            =
                    this._mapper.Map<GeneroIdNomeDto[]>(generos);

                return Ok(new {entretenimentos = entretenimentosDto, generos = generosDto});
            }catch(Exception exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, 
                    $"Banco de dados falhou {exception}");
            }
        }

        // pega todos os entretenimentos (id, nome) e todos os gêneros (id, nome) que estão relacionados

        // [HttpGet]
        // public async Task<IActionResult> Get()
        // {
        //     try 
        //     {
        //         var entretenimentosGeneros        = await this._context.GetEntretenimentosGenerosRelacionados();

        //         var results = this._mapper.Map<EntretenimentoGeneroIdNomeDto[]>(entretenimentosGeneros);

        //         List<EntretenimentoGeneroIdNomeDtoContainer> entretenimentosGenerosHateoas = 
        //             new List<EntretenimentoGeneroIdNomeDtoContainer>();

        //         foreach(var result in results)
        //         {
        //             EntretenimentoGeneroIdNomeDtoContainer entretenimentoGeneroHateoas = 
        //                 new EntretenimentoGeneroIdNomeDtoContainer();

        //             Link[] links = this._hateoas.GetActions(result.EntretenimentoId.ToString());

        //             entretenimentoGeneroHateoas.EntretenimentoGeneroIdNomeDto = result;
        //             entretenimentoGeneroHateoas.Links  = links;

        //             // Last() pega o último elemento (link) do array links
        //             if(links.Last().Method.Equals("DELETE", StringComparison.InvariantCultureIgnoreCase))
        //                 links.Last().Href = links.Last().Href + "/" + result.GeneroId;
                    
        //             entretenimentosGenerosHateoas.Add(entretenimentoGeneroHateoas);
        //         }

        //         return Ok(entretenimentosGenerosHateoas);
        //         // return Ok(new {entretenimentos = resultsEntretenimentos, generos = resultsGeneros});
        //     }catch(Exception exception)
        //     {
        //         return this.StatusCode(StatusCodes.Status500InternalServerError, 
        //             $"Banco de dados falhou {exception}");
        //     }
        // }

        // pega os gêneros do entretenimento passado

        [HttpGet("{idEntretenimento}")]
        public async Task<IActionResult> Get(int idEntretenimento) 
        {
            try 
            {
                var entretenimentoGeneros        = await this._context.GetGenerosFromEntretenimentos(idEntretenimento);
                var resultsEntretenimentoGeneros = this._mapper.Map<EntretenimentoGeneroDto[]>(entretenimentoGeneros);

                return Ok(resultsEntretenimentoGeneros);

            }catch(Exception exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                    $"Banco de dados falhou {exception}");
            }
        }

        // insere um id de entretenimento e um id de gênero na tabela entretenimentosgeneros

        [HttpPost]
        public async Task<IActionResult> Post(EntretenimentoGeneroDto model)
        {
            try 
            { 
                var eg = this._mapper.Map<EntretenimentoGenero>(model);
                this._context.Add(eg);

                if(await this._context.SaveChangesAsync())
                   return Created("/api/entretenimentogenero", this._mapper.Map<EntretenimentoGeneroDto>(eg));

            }catch(Exception exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, 
                    $"Banco de dados falhou {exception.Message}");
            }

            return BadRequest();
        }

        // deleta algum gênero de algum entretenimento

        [HttpDelete("{idEntretenimento}/{idGenero}")]
        public async Task<IActionResult> Delete(int idEntretenimento, int idGenero)
        {
            try 
            {   
                var entretenimentoGenero = await this._context.GetEntretenimentoGenero(idEntretenimento, idGenero);
                
                if (entretenimentoGenero == null) return NotFound();
                
                this._context.Delete(entretenimentoGenero);

                if(await this._context.SaveChangesAsync())
                    return Ok();

            }catch(Exception exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                    $"Banco de dados falhou {exception.Message}");
            }

            return BadRequest();
        }
    }
}