using System.Text;
using System.Linq;
using System;
using System.Threading.Tasks;
using AutoMapper;
using CatalogoVirtual_API.Dtos;
using CatalogoVirtual_API.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using CatalogoVirtual_API.DtosHateoas;

namespace CatalogoVirtual_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController: ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IMapper _mapper;
        private readonly Hateoas.Hateoas _hateoas;
        public UserController(IConfiguration config, UserManager<User> userManager,
                              SignInManager<User> signInManager, IMapper mapper)
        {
            this._config        = config;
            this._userManager   = userManager;
            this._signInManager = signInManager;
            this._mapper        = mapper;

            this._hateoas = new Hateoas.Hateoas("localhost:5000/api/user");
            this._hateoas.AddAction("GET_USER", "GET");
            this._hateoas.AddAction("PUT_USER", "PUT");
        }

        // dados do usuário após registro
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try 
            {
                var users    = this._userManager.Users;
                var usersDto = this._mapper.Map<UserDto[]>(await users.ToArrayAsync());
                
                List<UserDtoContainer> usersHateoas = new List<UserDtoContainer>();

                foreach(var userDto in usersDto)
                {
                    UserDtoContainer userHateoas = new UserDtoContainer();
                    userHateoas.UserDto          = userDto;
                    userHateoas.Links            = this._hateoas.GetActions(userDto.Id.ToString());

                    usersHateoas.Add(userHateoas);
                }
                
                return Ok(usersHateoas);
                // return Ok(usersDto);
            }catch(Exception exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                    $"Banco de dados falhou {exception}");
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var user                      = await this._userManager.FindByIdAsync(id.ToString());

                if(user == null) return NotFound();

                var userDto                   = this._mapper.Map<UserDto>(user);

                UserDtoContainer userHateoas  = new UserDtoContainer();
                userHateoas.UserDto           = userDto;
                userHateoas.Links             = this._hateoas.GetActions(userDto.Id.ToString());

                return Ok(userHateoas);

            }catch(Exception exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                    $"Banco de dados falhou {exception}");
            }
        }

        [HttpPut("{idUser}")]
        public async Task<IActionResult> Put(int idUser, UserDto model)
        {
            try 
            {
                var user = await this._userManager.FindByIdAsync(idUser.ToString());

                if(user == null) return NotFound();

                this._mapper.Map(model, user);

                await this._userManager.UpdateAsync(user);
                
                return Created("/api/user", this._mapper.Map<UserDto>(user));
            }catch(Exception exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                    $"Banco de dados falhou {exception}");
            }
        }

        [HttpDelete("{idUser}")]
        public async Task<IActionResult> Delete(int idUser) 
        {
            try
            {
                var user = await this._userManager.FindByIdAsync(idUser.ToString());

                if(user == null) return NotFound();

                await this._userManager.DeleteAsync(user);

                return Ok();
            }
            catch (Exception exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                    $"Banco de dados falhou {exception}");
            }
        }

        // registro do usuário

        [HttpPost("register")]
        [AllowAnonymous]
        public async Task<IActionResult> Register(UserDto userDto)
        {
            try {

                /*
                    na tabela User do bd não tem apenas os campos de UserDto, por isso
                    faremos mapeamento de dto para domínio
                */

                var user = this._mapper.Map<User>(userDto);
                var result = await this._userManager.CreateAsync(user, userDto.Password);

                if(result.Succeeded)
                    return Created("/api/user", this._mapper.Map<UserDto>(user));

            }catch(Exception exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                    $"Banco de dados falhou {exception}");
            }

            return BadRequest();
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login(UserLoginDto userLoginDto)
        {
            try 
            {
                // verifica se o UserName existe no bd
                var user = await this._userManager.FindByNameAsync(userLoginDto.UserName);

                if(user == null) return NotFound();

                // verifica se a senha existe no bd
                var result = await this._signInManager.CheckPasswordSignInAsync(user,
                    userLoginDto.Password, false);

                if(result.Succeeded)
                {
                    // pegaremos os dados do usuário que logou-se na tabela Users
                    var loggedInUser = await this._userManager.Users.FirstOrDefaultAsync(u => 
                        u.NormalizedUserName == userLoginDto.UserName.ToUpper());

                    // retorna UserName e Password
                    var userToReturn = this._mapper.Map<UserLoginDto>(loggedInUser);

                    return Ok(new {user = userToReturn, jwt = JWToken.Generate(
                        loggedInUser, this._config)});
                }

                return Unauthorized();

            }catch(Exception exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                    $"Banco de dados falhou {exception}");
            }
        }
    }
}