using Microsoft.AspNetCore.Identity;

namespace CatalogoVirtual_API.Identity
{
    public class UserRole : IdentityUserRole<int>
    {
        /*
            não preciso especificar as chaves UserId e RoleId, só o relacionamento entre User e Role
            já que UserRole está herdando de IdentityUserRole
        */

        public User User {get; set;}
        public Role Role {get; set;}
    }
}