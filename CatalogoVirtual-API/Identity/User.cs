using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace CatalogoVirtual_API.Identity
{ 
    public class User : IdentityUser<int>
    {
        [Column(TypeName="nvarchar(100)")]
        public string FullName {get; set;}
        
        public List<UserRole> UsersRoles {get; set;}
    }
}