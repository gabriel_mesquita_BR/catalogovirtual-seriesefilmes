using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace CatalogoVirtual_API.Identity
{
    public class Role : IdentityRole<int>
    {
        public List<UserRole> UsersRoles {get; set;}
    }
}